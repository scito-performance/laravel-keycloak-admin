<?php
namespace Scito\Laravel\Keycloak\Admin\Tests;

use Illuminate\Foundation\Testing\WithFaker;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Scito\Keycloak\Admin\Client;
use Scito\Laravel\Keycloak\Admin\Facades\KeycloakAdmin;
use Scito\Keycloak\Admin\Resources\RolesResource;
use Scito\Keycloak\Admin\Resources\UsersResource;
use RuntimeException;

class KeycloakFacadeTest extends TestCase
{
    use WithFaker, MockeryPHPUnitIntegration;

    /**
     * @test
     */
    public function the_keycloak_facade_generates_a_client(): void
    {
        $client = KeycloakAdmin::connection('master');
        $this->assertInstanceOf(Client::class, $client);
    }

    /**
     * @test
     */
    public function the_default_realm_is_used_for_the_users_resource_when_no_realm_is_specified(): void
    {
        $resource = KeycloakAdmin::users();
        $this->assertInstanceOf(UsersResource::class, $resource);
    }

    /**
     * @test
     */
    public function the_default_realm_is_used_for_the_roles_resource_when_no_realm_is_specified(): void
    {
        $resource = KeycloakAdmin::roles();
        $this->assertInstanceOf(RolesResource::class, $resource);
    }

    /**
     * @test
     */
    public function the_pipeline_can_be_used_to_set_the_ssl_verify_option_to_false(): void
    {
        $verify = true;
        // Create an instance with a pipe which sets the verify option to false
        // use the callback to get the resulting config
        KeycloakAdmin::configureGuzzleClientThrough(function (array $options) {
            $options['verify'] = false;

            return $options;
        })->afterConnectionCreated(function ($name, Client $client) use (&$verify) {
            $verify = $client->getResourceFactory()->getClient()->getConfig('verify');
        })->connection('master');

        $this->assertFalse($verify);
    }

    /**
     * @test
     */
    public function the_guzzle_client_can_be_configured_using_the_connections_of_the_config_file(): void
    {
        $this->app['config']->set('keycloak-admin.connections.master.guzzle', [
            'verify' => false,
        ]);

        $config = [];
        // Create an instance with a pipe which sets the verify option to false
        // use the callback to get the resulting config
        KeycloakAdmin::afterConnectionCreated(function ($name, Client $client) use (&$config) {
            $config = $client->getResourceFactory()->getClient()->getConfig();
        })->connection('master');

        $this->assertFalse($config['verify']);
    }

    /**
     * @test
     */
    public function a_exception_is_thrown_when_the_user_resource_is_accessed_directly_without_setting_a_default_realm(): void
    {
        $this->app['config']->set('keycloak-admin.connections.master', [
            'url' => 'http://keycloak:8080'
        ]);

        $this->expectException(RuntimeException::class);

        KeycloakAdmin::users();
    }

    /**
     * @test
     */
    public function a_exception_is_thrown_when_the_username_is_not_set_in_the_configuration(): void
    {
        $this->app['config']->set('keycloak-admin.connections.master', [
            'url' => 'http://keycloak:8080',
            'password' => $this->faker->password,
            'realm' => $this->faker->slug
        ]);

        $this->expectException(RuntimeException::class);

        KeycloakAdmin::roles();
    }

    /**
     * @test
     */
    public function a_exception_is_thrown_when_the_password_is_not_set_in_the_configuration(): void
    {
        $this->app['config']->set('keycloak-admin.connections.master', [
            'url' => 'http://keycloak:8080',
            'username' => $this->faker->userName,
            'realm' => $this->faker->slug
        ]);

        $this->expectException(RuntimeException::class);

        KeycloakAdmin::roles();
    }

    /**
     * @test
     */
    public function a_exception_is_thrown_when_the_realm_is_not_set_in_the_configuration(): void
    {
        $this->app['config']->set('keycloak-admin.connections.master', [
            'url' => 'http://keycloak:8080',
            'username' => $this->faker->userName,
            'password' => $this->faker->password
        ]);

        $this->expectException(RuntimeException::class);

        KeycloakAdmin::roles();
    }
}
