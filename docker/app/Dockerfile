FROM ubuntu:22.04

MAINTAINER Maikel van Maurik

# Set non-interactive mode to avoid prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Update and install locales
RUN apt-get clean && apt-get update && apt-get install -y locales \
    && locale-gen en_US.UTF-8 \
    && locale-gen nl_NL.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Set timezone to UTC
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install necessary packages and PHP 8.0
RUN apt-get update \
    && apt-get install -y software-properties-common curl zip unzip git sqlite3 nginx supervisor wget sudo apt-utils \
    && add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y php8.0 php8.0-fpm php8.0-cli php8.0-curl php8.0-dom php8.0-zip php8.0-memcached php8.0-mbstring php8.0-xdebug \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && mkdir /run/php \
    && apt-get remove -y --purge software-properties-common \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && echo "daemon off;" >> /etc/nginx/nginx.conf \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# Copy PHP configuration
COPY php-fpm.conf /etc/php/8.0/fpm/php-fpm.conf

# Expose port 80
EXPOSE 80

# Copy Supervisor configuration
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Start Supervisor
CMD ["/usr/bin/supervisord"]
